import { expect } from "chai";
import { Model } from "../src/model";
import fs from "fs/promises";


describe("The Model module", () => {

    const M = new Model(process.cwd(), "testDB.json");
    
    beforeEach(() => {
        console.log("blabla");
        fs.writeFile(`${process.cwd()}/${"testDB.json"}`, []);
    });
    

    context("#load", () => {
        it("should exist", () => {
            expect(M.load).to.be.a("function");
        });
        it("should load empty tasks array", async () => {
            const tasks = await M.load();
            expect(tasks).to.eql([]);
        });
    });

    context("#save", () => {
      
        it("should write to do list to DB"), () => {
            console.log("1");
           /*  M.save([{id :"0", description :"hello world",completed : false} ])
            .then(() => {
             console.log("reading saved file");
             return fs.readFile(`${process.cwd()}/${"testDB.json"}`,"utf8");

            })
            .then(rawData =>{
                expect( JSON.parse(rawData))
                .to.eql([{id :"0", description :"hello world",completed : false}]);
            }); */
            expect([1,2,3]).to.deep.equal([1,2,3]);
         };
      
      
      
        it("should exist", () => {
            expect(M.save).to.be.a("function");
        });
        it("should exist2", () => {
            expect(M.save).to.be.a("function");
        });

        /*it("should write to do list to DB"),async () => {
           console.log("1");
            await M.save([{id :"0", description :"hello world",completed : false} ]);
           console.log("123");
           const rawData = await fs.readFile(`${process.cwd()}/${"testDB.json"}`,"utf8");
           expect(await JSON.parse(rawData)).
           to.eql([{id :"0", description :"hello world",completed : false}]);
           
        };*/

        
    });
});
