export interface Todo {
    id: string;
    description: string;
    completed: boolean;
}

export enum Command {
    Create = "CREATE",
    Read = "READ",
    Update = "UPDATE",
    Remove = "REMOVE",
    Clear = "CLEAR",
    Help = "HELP",
}

export enum Flag {
    all = "a",
    completed = "c",
    open = "o",
}

export type ParseResult = {
    commandes: string[];
    flags: string[];
    args: string[];
};
