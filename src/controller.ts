import log from "@ajar/marker";
import { generateID } from "./utils.js";
import { Todo, Command, Flag, ParseResult } from "./types.js";
import { Model } from "./model.js";

export class Contoller {
    //class or module ?


    control(parseResult: ParseResult, list: Todo[]) {
        const [action, flags, args] = [
            parseResult.commandes[0],
            parseResult.flags,
            parseResult.args,
        ];
        switch (action) {
            case Command.Create:
                this.addTodo(list, args.join(" "));
                break;

            case Command.Read:
                this.handleRead(list, flags);
                break;

            case Command.Remove:
                this.removeFromList(list, args[0]);
                console.log(`removed task:${args[0]} from the list`);
                break;

            case Command.Update:
                this.toggleCompleted(list, args[0]);
                break;
            case Command.Clear:
                list = list.filter((item) => item.completed === false);
                break;

            case Command.Help:
                Contoller.printHelp();
                break;
            default:
                Contoller.printHelp();
        }
    }

    addTodo(list: Todo[], description: string): void {
        if (!description) {
            throw new Error("please describe the task");
        }
        const todo: Todo = { description, completed: false, id: generateID() };
        list.push(todo);
    }

    toggleCompleted(list: Todo[], id: string): void {
        const checked = list.find((todo) => todo.id === id);
        if (checked) {
            checked.completed = !checked.completed;
        }
    }

    removeFromList(list: Todo[], id: string): Todo[] {
        const idx = list.findIndex((elem) => elem.id === id);
        idx !== -1 ? list.splice(idx, 1) : "";
        return list;
    }

    static printHelp(): void {
        log.green(
            `Task Manager by Dor - Please use one of this commandes:
    ${Command.Create} -- Creates a new task with @param1 as description ,
    ${Command.Read} -- Prints all the tasks ,
        flags:
            -- a -> print all the tasks.
            -- o -> print only open/active tasks.
            -- c -> print only completed tasks.
            
    ${Command.Remove} -- Remove the task with id @param1,
    ${Command.Update} -- Toggle the completed fields of the task with id @param1  
    ${Command.Clear} -- Clear all the completed tasks
    `
        );
    }

    handleRead(todos: Todo[], flags: string[]) {
        if (flags.length) {
            switch (flags[0]) {
                case "a":
                    console.table(todos);
                    break;
                case "c":
                    console.table(todos.filter((task) => task.completed));
                    break;
                case "o":
                    console.table(
                        todos.filter((task) => task.completed === false)
                    );
                    break;
                default:
                    Contoller.printHelp();
            }
        } else {
            console.table(todos);
        }
    }
}
