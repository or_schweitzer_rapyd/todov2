import { Todo } from "./types.js";
import { Model } from "./model.js";
import { Contoller } from "./controller.js";
import { parse } from "./parser.js";
import { CliTodosValidate } from "./validator.js";

async function init() {
    //const input=process.argv.slice(2);
    const input = process.argv;
    const parseResult = parse(input);
    console.log(parseResult);
    try {
        const isVaild = CliTodosValidate(parseResult);
    } catch (err: any) {
        console.error(err.message);
        Contoller.printHelp();
        return;
    }

    const path = process.cwd();
    const mycontroler = new Contoller();
    const myDbManager = new Model(path, "DataBase.json");

    const todos: Todo[] = await myDbManager.load();
    mycontroler.control(parseResult, todos);
    await myDbManager.save(todos);
}

init();
